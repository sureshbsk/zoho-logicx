package com.zoho.cab;

public class CabModel {
	
	private String cabName;
	private String currentPoint;
	private int nextPickupTime;
	private int totalEarning;
	
	public CabModel(String cabName, String currentPoint, int nextPickupTime, int totalEarning) {
		super();
		this.cabName = cabName;
		this.currentPoint = currentPoint;
		this.nextPickupTime = nextPickupTime;
		this.totalEarning = totalEarning;
	}
	public String getCabName() {
		return cabName;
	}
	public void setCabName(String cabName) {
		this.cabName = cabName;
	}
	public String getCurrentPoint() {
		return currentPoint;
	}
	public void setCurrentPoint(String currentPoint) {
		this.currentPoint = currentPoint;
	}
	public int getNextPickupTime() {
		return nextPickupTime;
	}
	public void setNextPickupTime(int nextPickupTime) {
		this.nextPickupTime = nextPickupTime;
	}
	public int getTotalEarning() {
		return totalEarning;
	}
	public void setTotalEarning(int totalEarning) {
		this.totalEarning = totalEarning;
	}
	
}

package com.zoho.cab;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class CabRepository {

	List<CabHistory> history = new ArrayList<CabHistory>();
	Map<String, CabModel> cabs = new HashMap<String, CabModel>();
	
	public CabRepository() {
		
		cabs.put("Taxi-1", new CabModel("Taxi-1", "A", 0, 0));
		cabs.put("Taxi-2", new CabModel("Taxi-2", "A", 0, 0));
		cabs.put("Taxi-3", new CabModel("Taxi-3", "A", 0, 0));
		cabs.put("Taxi-4", new CabModel("Taxi-4", "A", 0, 0));				
	}
	
	public String getCab(String source, String destiny, int pickup) {
		
		String bookingStatus="";
		
		return "";
	}
	
	private int getEarning(String cabName) {
		CabModel cab = cabs.get(cabName);
		return cab.getTotalEarning();
	}
	
	private int getFare(String source, String destiny) {
		int distance = (int)destiny.charAt(0) - (int)source.charAt(0);
		distance *= 15 - 5;
		int fare =  100 + distance * 10;
		return fare;
	}
	
}

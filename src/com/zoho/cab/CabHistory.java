package com.zoho.cab;

public class CabHistory {
	
	private int bookingID;
	private int customerID;
	private String source;
	private String destination;
	private int pickupTime;
	private int dropTime;
	private int totalEarning;
	
	public int getBookingID() {
		return bookingID;
	}
	public void setBookingID(int bookingID) {
		this.bookingID = bookingID;
	}
	public int getCustomerID() {
		return customerID;
	}
	public void setCustomerID(int customerID) {
		this.customerID = customerID;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public int getPickupTime() {
		return pickupTime;
	}
	public void setPickupTime(int pickupTime) {
		this.pickupTime = pickupTime;
	}
	public int getDropTime() {
		return dropTime;
	}
	public void setDropTime(int dropTime) {
		this.dropTime = dropTime;
	}
	public int getTotalEarning() {
		return totalEarning;
	}
	public void setTotalEarning(int totalEarning) {
		this.totalEarning = totalEarning;
	}
	
	
}
